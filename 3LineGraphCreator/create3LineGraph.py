########################################################################
# create3LineGraph
#	by Zachary Holland
#
#	-->Program written for a coworker at HydromaxUSA to parse over excel
#	   to create a graph for the pipe data that was obtained
#		--Foot intervals of the pipe
#		--Water level
#		--Sediment
#		--Diameter of the pipe
#
#
########################################################################
import matplotlib.pyplot as plt
import numpy.core._methods
import numpy.lib.format
import openpyxl
import os, getpass

class create3LineGraph:
	graphCount = 0
	def __init__(self):
		# Lists for holding the data to be used in the graph
		self.feet = []
		self.waterLvl = []
		self.sediment = []
		self.diameter = []
		#Graph counter to keep track of how many graphs have been created
		create3LineGraph.graphCount +=1
		# Create folder to contain the generated graphs
		self.desktopPath = os.path.join('C:\\Users\\'+getpass.getuser()+'\\Desktop\\')
		if not os.path.exists(self.desktopPath+'Python Generated Graphs'):
			graphFldr = os.path.join(self.desktopPath+'Python Generated Graphs')
			os.mkdir(graphFldr)
			self.pathway = graphFldr
		else:
			self.pathway = os.path.join(self.desktopPath+'Python Generated Graphs')
	########################################################################
	# Process Excel data sheet
	#
	def parseExcel(self):
		fileLocation = input("Please drag the excel file containing the data to the cmd(press enter when done)\n:")
		# Load the excel file to parse the data from
		wb = openpyxl.load_workbook(fileLocation)
		# Get the excel sheet that is currently open(in this case we only have 1)
		dataSheet = wb.get_active_sheet()

		# Go through each row of data and append the values to the unique list
		for row in range(2,dataSheet.max_row):
			#Get FT values
			self.feet.append(dataSheet['{}{}'.format('A',row)].value)
			#Get water levels
			self.waterLvl.append(dataSheet['{}{}'.format('B',row)].value)
			#Get sediment
			self.sediment.append(dataSheet['{}{}'.format('C',row)].value)
			#Get diameter
			self.diameter.append(dataSheet['{}{}'.format('D',row)].value)

	########################################################################
	# Create the Graph
	#
	def generateGraph(self):
		# Create a figure that is 1x1 plots and select the first plot to write to
		fig = plt.figure()
		ax = fig.add_subplot(1,1,1)

		# Create a grid for both the x ticks and y ticks
		ax.grid(which='both') 
		# Set y limits
		plt.ylim(0, 90)
		#
		plt.plot(self.feet, self.waterLvl)
		plt.plot(self.feet, self.sediment)
		plt.plot(self.feet, self.diameter)
		plt.legend(['Water Level(in)', 'Sediment(in)', 'Diameter'], loc='upper right')
		plt.xlabel('Feet')
		#plt.ylabel('Inches')
		# Show the plot that was created
		#plt.show()

		# Save the plot as a png, if graphs have already been generated 
		# increment to the next graph number that hasnt been made yet
		while os.path.exists('{}{:d}.png'.format(self.pathway+"\\graph",create3LineGraph.graphCount)):
			create3LineGraph.graphCount += 1
		plt.savefig('{}{:d}.png'.format(self.pathway+"\\graph",create3LineGraph.graphCount))
		print("Graph"+str(create3LineGraph.graphCount)+".png has been created!")

if __name__ == "__main__":
	valid = True
	while valid:
		lineGraph = create3LineGraph()
		lineGraph.parseExcel()
		lineGraph.generateGraph()
		exitPrompt = input("\nWould you like to generate another graph? (press enter to continue/type no to quit)\n:")
		if exitPrompt == ("no"):
			valid = False
		else: 
			continue
