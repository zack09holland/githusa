#######################################################################
# proxPntDateChange
#
#######################################################################
# Author: Zachary Holland
#
#	Coordinates:
#		1341, 84	--create features button
#		1309, 216	--R click, clear prox point
#			1309, 305	--properties in R click
#		1309, 238	--R click, notclear prox point
#			1309, 339	--properties in R click
#		1309, 253	--R click, review prox point
#			1309, 440	--properties in R click
#		882, 440	--Double click, drop down calendar
#			783, 597	--todays date option
#
#
#######################################################################
import os, sys, subprocess, time
import pyautogui
from datetime import date

########################################################################
# Module for clicking
#
def clickHere(moveToX,moveToY,numClicks,clicksASecond,bttnOption):
	pyautogui.click(x=moveToX, y=moveToY, 
					clicks=numClicks, 
					interval=clicksASecond, 
					button=bttnOption)

#  Open up the field map
#os.startfile('C:\\PSE GIS\\Field Map.mxd')
#time.sleep(90)
########################################################################
#	Commands to start the editing process
#	- Afterwards it opens up the create features tab 
#	  and edits the dates to today
#

#Click on editor
clickHere(983,83,1,.5,"left")
# Click on start editing
clickHere(986,107,1,.5,"left")
# Click in screen popup
clickHere(537,270,1,.5,"left")
# Press 'P' to select Prox Clear file
pyautogui.press('p')
pyautogui.press('enter')
time.sleep(5)

#Click to open up the create features
clickHere(1341,84,1,.5,"left")

#Double click on CLEAR prox point
pyautogui.moveTo(1309,219)
clickHere(1309,219,2,.1,"left")
#Change the date to today
clickHere(882,440,2,1,"left")
clickHere(783,596,1,.5,"left")
pyautogui.press('enter')


#Double click on NOT CLEAR prox point
pyautogui.moveTo(1309,238)
clickHere(1309,238,2,.25,"left")
#Change the date to today
clickHere(882,440,2,1,"left")
clickHere(783,596,1,.5,"left")
pyautogui.press('enter')


# Create a Date Object
d = date.today()
print(d.strftime("%m/%d/%y"))
