########################################################################
# ftpTracker(class:FtpUploadTracker)
#
#
########################################################################
class FtpUploadTracker:
    sizeWritten = 0
    totalSize = 0
    lastShownPercent = 0

    def __init__(self, totalSize):
        self.totalSize = totalSize

    def handle(self, block):
        self.sizeWritten += 1024
        percentComplete = round((self.sizeWritten / self.totalSize) * 100)
    
        if (self.lastShownPercent != percentComplete):
            self.lastShownPercent = percentComplete
            print("    "+str(percentComplete) + "% complete",end='\r')