################################################################################################################################################
# exportProxClear
#
#
#	**run program while arcGIS is open
#	-create folder for ProxClear_ZH_YYYYMMDD
#	-create variable name for Prox_Clear_ZH_YYYYMMDD
#	--right click on 'Prox_Clear..' data layer
#	--export data
#	--navigate to created folder
#	--save shapefile with variable name
#
# ***No longer used at company due to switching from ArcMap on the computer to the Explorer app on our work phones. ***
#
#	--> MousePositionTracker.py was to be used to clean up the basic attempt I did for automating the clicks by allowing the user
#		to choose where they wanted to click vs predetermining where the program would click.
#
################################################################################################################################################
import pyautogui, os, time, sys, getpass
import shutil
import pyperclip
from ftpConnection import ftpConnector
from ftpTracker import FtpUploadTracker
#from MousePositionTracker import MouseClickTracker


class exportProxClear:

	def __init__(self):
		desktopPath = os.path.join('C:\\Users\\'+getpass.getuser()+'\\Desktop\\')
		#Create the desktop folder to hold the created proximity clear files
		#	-If already created set the pathway to be that of the folder on the desktop
		if not os.path.exists(desktopPath+'Proximity Clear Files'):
			prxClrFldr = os.path.join(desktopPath+'Proximity Clear Files')
			os.mkdir(prxClrFldr)
			self.pathway = prxClrFldr
		else:
			self.pathway = os.path.join(desktopPath+'Proximity Clear Files')
		self.todaysDate = time.strftime("%Y%m%d")
		self.proxClearName = "Prox_ZH_"+str(self.todaysDate)
		#Folder path that will be uploaded to the ftp
		self.uploadFldrPath = os.path.join(self.pathway+'\\'+self.proxClearName)
		if not os.path.exists(self.uploadFldrPath):
			# Create the savePath which will be used to save our 
			# new consolidate file
			os.mkdir(self.uploadFldrPath)
		else:
			# Remove the old folder and create a new one
			shutil.rmtree(self.uploadFldrPath, ignore_errors=True)
			os.mkdir(self.uploadFldrPath)
		#Save path for the particular shapefile being exported
		self.savePath = str(self.uploadFldrPath+'\\'+self.proxClearName+'.shp')

	#
	# Module for clicking
	#
	def clickHere(self,moveToX,moveToY,numClicks,clicksASecond,bttnOption):
		pyautogui.click(x=moveToX, y=moveToY, 
					clicks=numClicks, 
					interval=clicksASecond, 
					button=bttnOption)
	#
	# Create folder to hold the consolidated file which will
	# then be uploaded onto the ftp (uploadFldrPath)
	# 
	def createUpldFolder(self):
		self.uploadFldrPath = os.path.join(path+'\\'+file_name)
		if not os.path.exists(self.uploadFldrPath):
			# Create the savePath which will be used to save our 
			# new consolidate file
			os.mkdir(self.uploadFldrPath)
	#
	# Export the proximity clear data to a new shapefile
	#
	def exportData(self):
		pyautogui.keyDown('alt')
		pyautogui.press('tab')
		pyautogui.keyUp('alt')
		# Right click on the Prox_Clear layer
		#	- d = select data
		#	- e = export data
		self.clickHere(114,192,1,1,"right")
		pyautogui.press('d')
		#time.sleep(1)
		pyautogui.press('e')
		time.sleep(2)
		# Click on the box to type and select all
		self.clickHere(647,424,1,1,"left")
		pyautogui.keyDown('ctrlleft')
		pyautogui.press('a')
		pyautogui.keyUp('ctrlleft')
		#pyautogui.press('tab')
		#pyautogui.press('tab')
		time.sleep(1)
		# Copy the pathway to the newly created .shp file
		# and paste using 'ctrl+v'
		pyperclip.copy(self.savePath)	
		pyautogui.keyDown('ctrlleft')
		pyautogui.press('v')
		pyautogui.keyUp('ctrlleft')
		pyautogui.press('enter')
		time.sleep(4)
		self.clickHere(831,472,1,1,"left")
	#
	# Upload the newly created .shp file to the FTP
	#
	def uploadToFTP(self,pathName):
		proxClearFtpPath = "Data Upload/Proximity Team/Staging_Prep_for_Consolidated_ZH"
		ftpSession = ftpConnector(proxClearFtpPath)
		ftpSession.uploadJustThis(pathName)
		ftpSession.quitFTP()

	def display(self):
		print ("Date: "+self.todaysDate)
		print ("ProxClear File Name: " +self.proxClearName)
		print ("UploadFldr Path: " +self.uploadFldrPath)
		
if __name__ == "__main__":
	data = exportProxClear()
	#data.createUpldFolder(data.pathway,data.proxClearName)
	data.exportData()
	#Display the data for the file/folder
	data.display()
	#time.sleep(2)
	pyautogui.keyDown('alt')
	pyautogui.press('tab')
	pyautogui.keyUp('alt')
	
	while 1:
		status = input("Are you ready to upload? Enter yes to upload, no to quit \n")
		if status == 'yes':
			data.uploadToFTP(data.uploadFldrPath)
			break
		elif status=='no':
			break
		else:
			print("Looks like you entered something wrong, try that again.")
			continue

	




